package com.pawel.java21demo;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LinkedListTests {

    @Test
    public void givenLinkedList() {
        List<String> linkedList = new LinkedList<>();
        linkedList.add("abc");
        linkedList.add("def");
        linkedList.add("ghi");
        linkedList.add("jkl");

        assertThat(linkedList.get(0)).isEqualTo("abc");
        assertThat(linkedList.get(1)).isEqualTo("def");
        assertThat(linkedList.get(2)).isEqualTo("ghi");
        assertThat(linkedList.get(3)).isEqualTo("jkl");
        assertThat(linkedList.getFirst()).isEqualTo("abc");
        assertThat(linkedList.getLast()).isEqualTo("jkl");

        linkedList.addFirst("000");
        linkedList.addLast("999");
        assertThat(linkedList.get(0)).isEqualTo("000");
        assertThat(linkedList.get(linkedList.size()-1)).isEqualTo("999");
    }

    @Test
    public void givenArrayList() {
        List<String> arrayList = new ArrayList<>();
        arrayList.add("abc");
        arrayList.add("def");
        arrayList.add("ghi");
        arrayList.add("jkl");

        assertThat(arrayList.get(0)).isEqualTo("abc");
        assertThat(arrayList.get(1)).isEqualTo("def");
        assertThat(arrayList.get(2)).isEqualTo("ghi");
        assertThat(arrayList.get(3)).isEqualTo("jkl");
        assertThat(arrayList.getFirst()).isEqualTo("abc");
        assertThat(arrayList.getLast()).isEqualTo("jkl");

        arrayList.addFirst("000");
        arrayList.addLast("999");
        assertThat(arrayList.get(0)).isEqualTo("000");
        assertThat(arrayList.get(arrayList.size()-1)).isEqualTo("999");

    }
}
