package com.pawel.java21demo;

import org.apache.commons.collections4.ListUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class UnmodifiableCollectionsTests {

    @Test
    public void givenUsingCommonsCollections_whenUnmodifiableListIsCreated_thenNotModifiable() {
        List<String> list = new ArrayList<>(Arrays.asList("one", "two", "three"));
        List<String> unmodifiableList = ListUtils.unmodifiableList(list);
        assertThrows(UnsupportedOperationException.class, () -> unmodifiableList.add("four"));
    }

    @Test
    public void givenImmutableMap_WhenPutNewEntry_ThenThrowsUnsupportedOperationException() {
        Map<String, String> immutableMap = Map.of("name1", "Michael", "name2", "Harry");
        assertThrows(UnsupportedOperationException.class, () -> immutableMap.put("name3", "Micky"));
    }

    @Test
    public void givenImmutableList_WhenPutNewElement_ThenThrowsUnsupportedOperationException() {
        List<String> immutableList = List.of("Michael", "Nina", "Harry");
        assertThrows(UnsupportedOperationException.class, () -> immutableList.add("Micky"));
    }
}
