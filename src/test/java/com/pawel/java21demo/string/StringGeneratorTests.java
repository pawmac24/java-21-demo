package com.pawel.java21demo.string;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StringGeneratorTests {

    @Test
    public void checkIfReverse() {
        String text = "This is my favourite String";
        String newText = StringGenerator.reverse(text);
        assertThat(newText).isEqualTo("gnirtS etiruovaf ym si sihT");
    }

    @Test
    public void checkIfTheSecondLetterUpper() {
        String text = "This is my favourite String";
        String newText = StringGenerator.makeTheSecondLetterUpper(text);
        assertThat(newText).isEqualTo("ThIs iS My fAvOuRiTe sTrInG");
    }
}
