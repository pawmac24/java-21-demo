package com.pawel.java21demo;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

public class FunctionTests {

    @Test
    public void test_Function_1() {
        Function<String, Integer> func = x -> x.length();
        Function<Integer, Integer> func2 = x -> x * 2;
        Integer result = func.andThen(func2).apply("stopka");   // 12
        assertThat(result).isEqualTo(12);
    }

    @Test
    public void test_list_to_map_converter() {
        ListToMapConverter converter = new ListToMapConverter();

        List<String> list = Arrays.asList("node", "c++", "java", "javascript");

        //Map<String, Integer> map = converter.toMap(list, x -> x.length());
        Map<String, Integer> map = converter.toMap(list, converter::getLength);
        System.out.println(map);    // {node=4, c++=3, java=4, javascript=10}
        assertThat(map.toString()).isEqualTo("{node=4, c++=3, java=4, javascript=10}");
    }
}
