package com.pawel.java21demo.collection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class CollectionTests {

    @Test
    public void whenAddExistingElement_shouldPreserveSize() {
        Set<String> mySet = new HashSet<>(Arrays.asList("a", "b", "c"));
        mySet.add("b");

        assertThat(mySet).hasSize(3);
        assertThat(mySet).contains("a", "b", "c");
    }

    @Test
    public void whenAddNewElement_shouldIncreaseSize() {
        Set<String> mySet = new HashSet<>(Arrays.asList("a", "b", "c"));
        mySet.add("d");

        assertThat(mySet).hasSize(4);
        assertThat(mySet).contains("a", "b", "c", "d");
    }

    @Test
    public void whenRemoveElement_shouldIDecreaseSize() {
        Set<String> mySet = new HashSet<>(Arrays.asList("a", "b", "c"));
        mySet.remove("a");

        assertThat(mySet).hasSize(2);
        assertThat(mySet).contains("b", "c");
        assertThat(mySet).doesNotContain("a");
    }

    @Test
    public void hashMapTest() {
        Map<String, Integer> letterCountMap = new HashMap<>();
        letterCountMap.put("a", 2);
        letterCountMap.put("b", 3);
        letterCountMap.put("c", 1);
        letterCountMap.put("d", 3);
        //
        letterCountMap.put("c", 10);
        System.out.println(letterCountMap);

        long sum = letterCountMap.entrySet().stream().map(Map.Entry::getValue).reduce(0, Integer::sum);
        assertThat(sum).isEqualTo(18);

        Integer sum2 = letterCountMap.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.summingInt(Integer::intValue));
        assertThat(sum2).isEqualTo(18);

        Integer sum3 = letterCountMap.entrySet().stream().map(Map.Entry::getValue).mapToInt(Integer::intValue).sum();
        assertThat(sum3).isEqualTo(18);
    }
}

