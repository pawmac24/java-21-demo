package com.pawel.java21demo;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OptionalTests {

    @Test
    public void givenAddressNull_whenCallOptionalOf_itThrowsNullPointerException() {
        String address = null;
        assertThrows(NullPointerException.class, () -> Optional.of(address));
    }

    @Test
    public void givenAddressNull_WhenCallOptionalOfNullable_itReturnsOptionalEmpty() {
        String address = null;
        Optional<String> addressOptional = Optional.ofNullable(address);
        assertThat(addressOptional).isEqualTo(Optional.empty());
        assertThat(addressOptional.isPresent()).isFalse();
    }

    @Test
    public void givenAddressIsSet_WhenCallOptionalOfNullable_itReturnsNoneEmptyOptional() {
        String address = "myAddress";
        Optional<String> addressOptional = Optional.ofNullable(address);
        addressOptional.ifPresent(name -> assertThat(name.length()).isEqualTo(9));
        assertThat(addressOptional.isPresent()).isTrue();
    }

    @Test
    public void givenAddressNull_When() {
        String valueOrNull = null;
        Optional.ofNullable(valueOrNull)
                .ifPresentOrElse(
                        (xx) -> System.out.println(xx),
                        () -> System.out.println("empty")
                );
    }

    @Test
    public void whenNullText_thenDefaultText() {
        String text = null;

        String defaultText = Optional.ofNullable(text).orElseGet(() -> "Default Value");
        assertEquals("Default Value", defaultText);

        defaultText = Optional.ofNullable(text).orElse( "Default Value");
        assertEquals("Default Value", defaultText);
    }
}
