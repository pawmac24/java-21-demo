package com.pawel.java21demo;

import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class LinkedHashMapTests {

    @Test
    public void givenLinkedHashMap_whenGetsOrderedKeySet_thenCorrect() {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<>();
        map.put(1, null);
        map.put(2, null);
        map.put(3, null);
        map.put(4, null);
        map.put(5, null);

        List<Integer> keyList = map.keySet().stream().collect(Collectors.toList());
        List<Integer> orderedKeys1 = IntStream.rangeClosed(1, 5).boxed().collect(Collectors.toList());
        List<Integer> orderedKeys2 = List.of(1, 2, 3, 4, 5);

        assertThat(keyList.equals(orderedKeys1)).isTrue();
        assertThat(keyList.equals(orderedKeys2)).isTrue();
    }
}
