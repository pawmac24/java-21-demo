package com.pawel.java21demo.component;

import org.springframework.stereotype.Component;

@Component
public class MyTranslator implements Translator{

    @Override
    public String translate(String input) {
        return "translate";
    }
}
