package com.pawel.java21demo.component;

public interface Translator {
    String translate(String input);
}
