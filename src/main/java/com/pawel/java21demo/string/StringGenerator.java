package com.pawel.java21demo.string;

public class StringGenerator {

    public static String reverse(final String myString) {
        char[] charArray = new char[myString.length()];
        int index = myString.length() - 1;
        for (char c : myString.toCharArray()) {
            charArray[index] = c;
            index--;
        }
        return new String(charArray);
    }

    public static String makeTheSecondLetterUpper(String myString) {
        char[] charArray = new char[myString.length()];
        int index = 0;
        for (char c : myString.toCharArray()) {
            char newChar = c;
            if (c == ' ') {
                charArray[index] = newChar;
                index++;
                continue;
            } else if (index % 2 == 0) {
                newChar = String.valueOf(c).toUpperCase().charAt(0);
            } else {
                newChar = String.valueOf(c).toLowerCase().charAt(0);
            }
            charArray[index] = newChar;
            index++;
        }
        return new String(charArray);
    }

}
