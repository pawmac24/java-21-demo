package com.pawel.java21demo.groupby;

import lombok.Value;

import java.util.IntSummaryStatistics;

@Value
public class BlogPost {
    String title;
    String author;
    BlogPostType type;
    int likes;

    record AuthPostTypesLikes(String author, BlogPostType type, int likes) {};
    record PostCountTitlesLikesStats(long postCount, String titles, IntSummaryStatistics likesStats) {};
    record TitlesBoundedSumOfLikes(String titles, int boundedSumOfLikes) {};
}