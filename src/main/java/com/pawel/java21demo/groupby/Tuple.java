package com.pawel.java21demo.groupby;

import lombok.Data;

@Data
public class Tuple {
    private final BlogPostType type;
    private final String author;
}
