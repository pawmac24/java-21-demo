package com.pawel.java21demo.groupby;

public enum BlogPostType {
    NEWS,
    REVIEW,
    GUIDE
}
